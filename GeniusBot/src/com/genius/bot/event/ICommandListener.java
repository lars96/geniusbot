package com.genius.bot.event;

public interface ICommandListener extends IListener {
	public void onCommand(CommandEventArgs args);
}
