package com.genius.bot.event;

public class CommandNotifier extends Notifier<ICommandListener> {

	public void setOnCommand(CommandEventArgs args) {
		for (ICommandListener i : getListeners()) {
			i.onCommand(args);
		}
	}
}
