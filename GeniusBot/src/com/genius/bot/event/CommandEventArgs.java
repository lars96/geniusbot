package com.genius.bot.event;

import java.util.List;

import com.genius.bot.core.Bot;
import com.genius.bot.core.Command;

import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

public class CommandEventArgs {

	private Bot bot;
	private Command command;
	private IMessage message;
	private List<String> params;
	private String raw_params;

	public CommandEventArgs(Bot bot, Command command, IMessage message, List<String> params, String raw_params) {
		this.bot = bot;
		this.command = command;
		this.message = message;
		this.params = params;
		this.raw_params = raw_params;
	}

	public Bot getBot() {
		return bot;
	}

	public Command getCommand() {
		return command;
	}

	public IMessage getMessage() {
		return message;
	}

	public IChannel getChannel() {
		return message.getChannel();
	}

	public IUser getIssuer() {
		return message.getAuthor();
	}

	public List<String> getParams() {
		return params;
	}

	public String getRawParams() {
		return raw_params;
	}
}
