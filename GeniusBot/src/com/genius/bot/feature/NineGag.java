package com.genius.bot.feature;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.genius.bot.core.Operations;
import com.genius.bot.event.CommandEventArgs;
import com.genius.bot.event.ICommandListener;

public class NineGag implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		if(args.getParams().size() == 0) {
			String src = getSource(args);
			Document doc = Jsoup.parse(src);
			Elements gifposts = doc.select(".badge-item-img");
			List<String> gifs = new ArrayList<String>();
			for(Element i : gifposts) {
				gifs.add(i.attributes().get("src"));
			}
			Random r = new Random();
			int index = r.nextInt(gifs.size() + 1);
			Operations.sendMessage(args, gifs.get(index));
		} else {
			Operations.sendMessage(args, args.getIssuer().mention() + ": Invalide Parameter!");
		}
	}
	
	public String getSource(CommandEventArgs args) {
		String src = "";
		try {
			URL url = new URL("https://9gag.com/fresh");
			URLConnection urlc = url.openConnection();
			BufferedReader bfr = new BufferedReader(new InputStreamReader(urlc.getInputStream(), Charset.forName("UTF-8")));
			String inputLine;
			StringBuilder srcb = new StringBuilder();
			while ((inputLine = bfr.readLine()) != null) {
				srcb.append(inputLine);
			}
			src = srcb.toString();
        	bfr.close();
		} catch (IOException e) {
			Operations.sendMessage(args, args.getIssuer().mention() + ": Fehler: " + e.getMessage());
			e.printStackTrace();
		}
		return src;
	}
	
}
