package com.genius.bot.feature;

import java.sql.SQLException;

import com.genius.bot.core.APIConnection;
import com.genius.bot.core.DbConnection;
import com.genius.bot.core.Operations;
import com.genius.bot.event.*;
import com.google.gson.JsonObject;

public class IQ implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		String msg; 
		String curIQ;
		String dbIQ;
		if (args.getParams().size() == 1) {
			String user = args.getParams().get(0);
			try{
				DbConnection dbcon =  Operations.getDbConnection();
				String s =  dbcon.getIQ(user);
				if (s == null){
					msg = "Nutzer " + user + " nicht in der Datenbank vorhanden!\nBitte via https://larsbutnotleast.xyz/geniusgraph/ registrieren.";
				}
				else {
					dbIQ = s;
					msg = "IQ f�r " + user + ":\n"
							+ dbIQ + "\t(via GeniusGraph)\n";
					
					String token = dbcon.getAuthToken(user);
					if (token != null) {
						APIConnection api = new APIConnection(token);
						JsonObject response = api.getRequest("account");
						if (response != null ){
							String display_iq = response.getAsJsonObject("response").getAsJsonObject("user").getAsJsonPrimitive("iq_for_display").getAsString();
							curIQ = display_iq;	
							msg += curIQ + "\t(via API)";
						}
					}

				}
				Operations.sendMessage(args, msg);
				
			}
			catch(SQLException e){
				Operations.sendMessage(args, "err " + args.getIssuer().mention() + ": " + e.getMessage());
				e.printStackTrace();
			} catch (Exception e) {
				Operations.sendMessage(args, "err " + args.getIssuer().mention() + ": " + e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
}