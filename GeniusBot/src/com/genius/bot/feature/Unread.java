package com.genius.bot.feature;

import com.genius.bot.core.APIConnection;
import com.genius.bot.core.DbConnection;
import com.genius.bot.core.Operations;
import com.genius.bot.event.*;
import com.google.gson.JsonObject;

public class Unread implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		String msg = null;

		DbConnection dbcon = Operations.getDbConnection();
		if (args.getParams().size() == 1) {
			String user = args.getParams().get(0);
			try {
				if (dbcon.checkGeniusUserPermission(args.getIssuer(), user)) {
					String token = dbcon.getAuthToken(user);
					if (token != null) {
						APIConnection api = new APIConnection(token);
						JsonObject response = null;
						response = api.getRequest("account");
				
						if (response != null) {
							String name = response.getAsJsonObject("response").getAsJsonObject("user").getAsJsonPrimitive("name").getAsString();
							int unread_general = response.getAsJsonObject("response").getAsJsonObject("user").getAsJsonPrimitive("unread_main_activity_inbox_count").getAsInt();
							int unread_msg = response.getAsJsonObject("response").getAsJsonObject("user").getAsJsonPrimitive("unread_messages_count").getAsInt();
							int unread_news = response.getAsJsonObject("response").getAsJsonObject("user").getAsJsonPrimitive("unread_newsfeed_inbox_count").getAsInt();
							int unread_groups = response.getAsJsonObject("response").getAsJsonObject("user").getAsJsonPrimitive("unread_groups_inbox_count").getAsInt();

							String str3 = Operations.formatUnread("Benachrichtigungen:", unread_general);
							String str4 = Operations.formatUnread("Nachrichten:", unread_msg);
							String str2 = Operations.formatUnread("Newsfeed:", unread_news);
							String str1 = Operations.formatUnread("Forum:", unread_groups);
							
							msg = "```Ungelesene Benachrichtigungen f�r " + Operations.stripOffNonPrintableCharacters(name) + ":\n"
									+ str1 + "\n"
									+ str2 + "\n"
									+ str3 + "\n"
									+ str4
									+ "```";	
						}
					}
					else
					{
						msg = "Der User " + user + " ist nicht registriert!";
					}
				}
				else 
				{
					msg = "Fehlende Berechtigung f�r diesen Genius-User!";
				}
			}
			catch (Exception e) {
				Operations.sendMessage(args, args.getIssuer().mention() + ": " + e.getMessage());
			}
		}	
		Operations.sendMessage(args, msg);	
	}				
}