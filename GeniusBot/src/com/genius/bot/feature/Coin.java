package com.genius.bot.feature;

import java.security.SecureRandom;
import java.util.Random;
import com.genius.bot.event.CommandEventArgs;
import com.genius.bot.event.ICommandListener;

public class Coin implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		if(args.getParams().size() == 0) {
			Random r = new SecureRandom();
			int otw = r.nextInt(2);
			if(otw == 0) {
				args.getChannel().sendMessage("Kopf!");
			} else if(otw == 1) {
				args.getChannel().sendMessage("Zahl!");
			}
		} else {
			args.getChannel().sendMessage(args.getIssuer().mention() + ": Ung�ltige Parameter!");
		}
	}

}
