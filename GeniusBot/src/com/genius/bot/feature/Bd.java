package com.genius.bot.feature;

import java.util.ArrayList;
import java.util.List;
import com.genius.bot.core.Bot;
import com.genius.bot.core.Constants;
import com.genius.bot.core.Operations;
import com.genius.bot.event.CommandEventArgs;
import com.genius.bot.event.ICommandListener;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.MessageHistory;
import sx.blah.discord.util.RateLimitException;

public class Bd implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		if(args.getParams().size() == 0) {
			if(args.getIssuer().getRolesForGuild(args.getChannel().getGuild()).contains(args.getChannel().getGuild().getRoleByID(Long.parseLong(Constants.getRoleID.ADMIN.toString())))) {
				Bot.client.getDispatcher().registerTemporaryListener(new IListener<MessageReceivedEvent>() {
					@Override
					public void handle(MessageReceivedEvent event) {
						if(event.getChannel().equals(args.getChannel())) {
							if(event.getAuthor().equals(args.getIssuer())) {
								if(event.getMessage().getContent().equalsIgnoreCase("J")) {
									IChannel ch = args.getChannel();
									List<IMessage> lm = new ArrayList<IMessage>();
									MessageHistory mhist = ch.getFullMessageHistory();
									for(IMessage i : mhist) {
										lm.add(i);
									}
									for(IMessage m : lm) {
										try {
											m.delete();
										} catch(RateLimitException e) {
											try {
												long de = e.getRetryDelay();
												Thread.sleep(de + 10);
												m.delete();
											} catch(InterruptedException ex) {
												ex.printStackTrace();
											}
										}
									}
								} else if(event.getMessage().getContent().equalsIgnoreCase("N")) {
									Operations.sendMessage(args, args.getIssuer().mention() + ": Abbruch ...");
								} else {
									Operations.sendMessage(args, args.getIssuer().mention() + ": Ung�ltige Antwort. Abbruch ...");
								}
							}
						}
					}
				});
				Operations.sendMessage(args, args.getIssuer().mention() + ": Dieser Befehl wird alle Nachrichten in diesem Channel l�schen und ist nicht abbrechbar. Bist du sicher, dass du alle Nachrichten in diesem Channel l�schen m�chtest? (j/n)");
			} else {
				Operations.sendMessage(args, args.getIssuer().mention() + ": Sorry, du hast keine Berechtigung, um diesen Befehl auszuf�hren!");
			}
		} else {
			Operations.sendMessage(args, args.getIssuer().mention() + ": Ung�ltige Parameter!");
		}
	}
}
