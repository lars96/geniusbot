package com.genius.bot.feature;

import com.genius.bot.core.Operations;
import com.genius.bot.event.*;

public class Ping implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		Operations.sendMessage(args, args.getIssuer().mention() + ", pong!");
	}
	
}