package com.genius.bot.feature;

import com.genius.bot.core.Operations;
import com.genius.bot.event.*;

public class PingPong implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		Operations.sendMessage(args, args.getIssuer().mention() + ", pongping!");
		
	}
	
}