package com.genius.bot.feature;

import com.genius.bot.core.DbConnection;
import com.genius.bot.core.LeaderboardParser;
import com.genius.bot.core.Operations;
import com.genius.bot.event.*;

import java.io.IOException;
import java.net.MalformedURLException;

public class LeaderboardsWeekly implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		String msg = null;
		try {
			if (args.getParams().size() == 1) {
				String tag = args.getParams().get(0);
				DbConnection dbcon = Operations.getDbConnection();
				int tagId = dbcon.getTag(tag.toLowerCase());
				if (tagId == 0){
					msg = "Tag " + tag + " existiert nicht!";
				}
				else
				{
					msg = getLeaderboards(tagId);
				}
			}
			else {
				msg = getLeaderboards();
			}		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (msg != null) {
			Operations.sendMessage(args, msg);
		}
	}
	
	private String getLeaderboards() throws Exception{
		return getLeaderboards(1658);
	}

	private String getLeaderboards(int tag) throws Exception {
		DbConnection dbcon = Operations.getDbConnection();
		String pos = "Top " + dbcon.getTagDescription(tag) + " User in den letzten Woche:\n\n";
		
		String leaderboards = LeaderboardParser.parseLeaderboards("week", tag);
		if (leaderboards == null) {
			leaderboards = "FEHLER!";
		}
		pos += leaderboards;
		return pos;
	}
}