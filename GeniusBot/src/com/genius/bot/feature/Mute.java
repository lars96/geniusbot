package com.genius.bot.feature;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import com.genius.bot.core.Operations;
import com.genius.bot.core.Privileges;
import com.genius.bot.event.CommandEventArgs;
import com.genius.bot.event.ICommandListener;
import com.vdurmont.emoji.EmojiManager;

public class Mute implements ICommandListener {
	
	public final String FILE_NAME = System.getProperty("user.dir") + File.separator + "mutes.dat";

	@Override
	public void onCommand(CommandEventArgs args) {
		if(Privileges.checkAdminPrivileges(args)) {
			if(args.getParams().size() == 1) {
				String name = args.getParams().get(0);
				Long id;
				try {
					id = Long.parseLong(name);
				} catch(NumberFormatException ex) {
					Operations.sendMessage(args, args.getIssuer().mention() + ": Ung�ltige ID!");
					return;
				}
				File mutelist = new File(FILE_NAME);
				if(args.getChannel().getGuild().getUserByID(id) != null) {
					try {
						if(mutelist.createNewFile()) {
							PrintWriter pw;
							try {
								pw = new PrintWriter(new FileOutputStream(FILE_NAME));
								pw.println(id);
							    pw.close();
							} catch (FileNotFoundException ex) {
								ex.printStackTrace();
								Operations.sendMessage(args, args.getIssuer().mention() + ": " + ex.getMessage());
								return;
							}
						} else {
							try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
							    String crl;
							    String sid = String.valueOf(id);
							    while ((crl = br.readLine()) != null) {
							       if(crl.equals(sid)) {
							    	   Operations.sendMessage(args, args.getIssuer().mention() + ": Dieser Nutzer ist bereits gemuted!");
							    	   br.close();
							    	   return;
							       }
							    }
							    br.close();
							} catch(Exception ex) {
								ex.printStackTrace();
								Operations.sendMessage(args, args.getIssuer().mention() + ": " + ex.getMessage());
								return;
							}
							List<String> mutedusers = new ArrayList<String>();
							try (BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
							    String crl;
							    while ((crl = br.readLine()) != null) {
							    	mutedusers.add(crl);
							    }
							    mutedusers.add(String.valueOf(id));
							    br.close();
				    	   } catch(Exception ex) {
				    		   ex.printStackTrace();
				    		   Operations.sendMessage(args, args.getIssuer().mention() + ": " + ex.getMessage());
				    		   return;
				    	   }
				    	   try {
				    		   mutelist.delete();
				    		   try {
				    			   mutelist.createNewFile();
				    			   PrintWriter pw;
				    			   try {
										pw = new PrintWriter(new FileOutputStream(FILE_NAME));
										for(String user : mutedusers) {
											pw.println(user);
										}
									    pw.close();
									} catch (FileNotFoundException ex) {
										ex.printStackTrace();
										Operations.sendMessage(args, args.getIssuer().mention() + ": " + ex.getMessage());
										return;
									}
				    		   } catch(IOException ex) {
				    			   ex.printStackTrace();
					    		   Operations.sendMessage(args, args.getIssuer().mention() + ": " + ex.getMessage());
					    		   return;
				    		   }
				    			   
				    	   } catch(SecurityException ex) {
				    		   ex.printStackTrace();
				    		   Operations.sendMessage(args, args.getIssuer().mention() + ": " + ex.getMessage());
				    		   return;
				    	   }
						}
					} catch (IOException ex) {
						ex.printStackTrace();
						Operations.sendMessage(args, args.getIssuer().mention() + ": " + ex.getMessage());
						return;
					}
					Operations.sendMessage(args, id + " ist jetzt " + EmojiManager.getForAlias("speak_no_evil").getUnicode() + ".");
				} else {
					Operations.sendMessage(args, args.getIssuer().mention() + ": Dieser Nutzer ist nicht auf diesem Discord-Server.");
				}
			} else {
				Operations.sendMessage(args, args.getIssuer().mention() + ": Ung�ltige Parameter!");
			}
		} else {
			Operations.sendMessage(args, args.getIssuer().mention() + ": Sorry, du darfst diesen Befehl nicht ausf�hren!");
		}
	}

}
