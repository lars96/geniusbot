package com.genius.bot.feature;

import com.genius.bot.core.APIConnection;
import com.genius.bot.core.DbConnection;
import com.genius.bot.core.Operations;
import com.genius.bot.event.*;
import com.google.gson.JsonObject;

public class Avatar implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		if (args.getParams().size() == 1) {
			String user = args.getParams().get(0);
			try {
				DbConnection dbcon = Operations.getDbConnection();
				String msg = null;
				String token = dbcon.getAuthToken(user);
				if (token != null) {
					APIConnection api = new APIConnection(token);
					JsonObject response = api.getRequest("account");
					if (response != null ){
						String avatar_link = response.getAsJsonObject("response").getAsJsonObject("user").getAsJsonObject("avatar").getAsJsonObject("medium").getAsJsonPrimitive("url").getAsString();
						msg = "Avatar f�r den Nutzer " + user + ": " + avatar_link;		
					}
				}
				else {
					msg = "Nutzer " + user + " nicht in der Datenbank vorhanden!";
				}

				Operations.sendMessage(args, msg);	
			}
			catch (Exception e) {
				Operations.sendMessage(args, "exception");
			}
		}
			
	}
	
}