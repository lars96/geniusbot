package com.genius.bot.feature;

import java.util.List;

import com.genius.bot.core.Operations;
import com.genius.bot.event.*;

import sx.blah.discord.handle.obj.IRole;

public class MyRoles implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		String msg = "Rollen f�r " + args.getIssuer().mention() +  ":\n";
		if (args.getParams().size() == 0) {
			List<IRole> li = args.getIssuer().getRolesForGuild(args.getChannel().getGuild());
			for(IRole i : li){
				msg += i.getName() + " (" + i.getStringID() + ")\n";
			}
			Operations.sendMessage(args, msg);
		}
		
	}
	
}