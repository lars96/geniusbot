package com.genius.bot.feature;

import java.util.Random;

import com.genius.bot.core.APIConnection;
import com.genius.bot.core.Operations;
import com.genius.bot.event.*;
import com.google.gson.JsonObject;

public class RandomTrack implements ICommandListener {

	private APIConnection api = null;
	
	public RandomTrack() {
		api = new APIConnection();
	}
	
	@Override
	public void onCommand(CommandEventArgs args) {
		String msg = "";
		if (args.getParams().size() == 0) {
			msg = getRandomTrack();
		}
		else
		{
			int count = Integer.parseInt(args.getParams().get(0));
			for (int i = 0; i < count; i++ ){
				msg += getRandomTrack() + "\n";
			}
		}
		Operations.sendMessage(args, msg);	
	}
	
	private String getRandomTrack() {
		JsonObject response = null;
		Random r = new Random();
		while (response == null) {
			int i = r.nextInt(100000) * 10;
			try {
				response = api.getRequest("songs/" + String.valueOf(i));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String path = response.getAsJsonObject("response").getAsJsonObject("song").getAsJsonPrimitive("path").getAsString();
		return "https://genius.com" + path;
	}
}