package com.genius.bot.feature;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import com.genius.bot.core.Operations;
import com.genius.bot.core.Privileges;
import com.genius.bot.event.CommandEventArgs;
import com.genius.bot.event.ICommandListener;

public class ListMutedUsers implements ICommandListener {
	
	public final String FILE_NAME = System.getProperty("user.dir") + File.separator + "mutes.dat";

	@Override
	public void onCommand(CommandEventArgs args) {
		if(Privileges.checkAdminPrivileges(args)) {
			String message = "Derzeit gemutete Nutzer:";
			File file = new File(FILE_NAME);
			if(file.exists()) {
				try(BufferedReader br = new BufferedReader(new FileReader(FILE_NAME))) {
					String crl;
					String cur_nick;
					if(br.readLine() == null) {
						message += "\nKeine.";
					} else {
						while((crl = br.readLine()) != null) {
							cur_nick = args.getChannel().getGuild().getUserByID(Long.parseLong(crl)).getName();
							message += "\n" + crl + " (" + cur_nick + ")";
						}
					}
				} catch(Exception ex) {
					ex.printStackTrace();
					Operations.sendMessage(args, args.getIssuer().mention() + ": " + ex.getMessage());
					return;
				}
			} else {
				message += "\nKeine.";
			}
			Operations.sendMessage(args, message);
		} else {
			Operations.sendMessage(args, args.getIssuer().mention() + ": Sorry, du darfst diesen Befehl nicht ausf�hren!");
		}
	}

}
