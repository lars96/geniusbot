package com.genius.bot.feature;

import com.genius.bot.core.Constants;
import com.genius.bot.core.DbConnection;
import com.genius.bot.core.Operations;
import com.genius.bot.core.RoleChecker;
import com.genius.bot.event.*;

public class Grant implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		if (args.getParams().size() == 2) {
			String discorduser = args.getParams().get(0).replaceAll("<", "").replaceAll(">", "").replaceAll("@", "").replaceAll("!", "");
			String geniususer = args.getParams().get(1);
		
			if (RoleChecker.check(args, Constants.getRoleID.ADMIN.toString())) {
				DbConnection dbcon = Operations.getDbConnection();
				dbcon.insertGeniusUserPermission(discorduser, geniususer);
			}
			else
			{
				Operations.sendMessage(args, args.getIssuer().mention() + ": Diese Operation darf nur durch einen Admin vorgenommen werden!");
			}
		} else {
			Operations.sendMessage(args, args.getIssuer().mention() + ": Invalide Parameter!");
		}
	}
	
}