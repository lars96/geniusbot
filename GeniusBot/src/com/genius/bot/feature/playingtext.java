package com.genius.bot.feature;

import com.genius.bot.core.Bot;
import com.genius.bot.core.Operations;
import com.genius.bot.core.Privileges;
import com.genius.bot.event.*;

public class playingtext implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		if(Privileges.checkAdminPrivileges(args)) {
			if(args.getParams().size() == 0) {
				Bot.client.changePlayingText(null);
			} else {
				String text = "";
				for(String i : args.getParams()) {
					text += i + " ";
				}
				text = text.substring(0, text.length() - 1);
				Bot.client.changePlayingText(text);
			}
		} else {
			Operations.sendMessage(args, args.getIssuer().mention() + ": Sorry, du bist nicht in der \"Administrator\"-Gruppe!");
		}
	}
}
