package com.genius.bot.feature;

import com.genius.bot.core.Operations;
import com.genius.bot.event.*;

public class Hallo implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		Operations.sendMessage(args, "Hi, " + args.getIssuer().mention() + "!");

	}
	
}