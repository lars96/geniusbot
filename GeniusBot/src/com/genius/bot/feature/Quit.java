package com.genius.bot.feature;

import com.genius.bot.core.Operations;
import com.genius.bot.core.Privileges;
import com.genius.bot.event.*;

public class Quit implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		String msg = null;
		boolean quit = false;
		
		quit = Privileges.checkAdminPrivileges(args) ? true : false;
		msg = quit ? args.getIssuer().mention() + ": Bot erfolgreich beendet." : args.getIssuer().mention() + ": Du bist nicht in der \"Administrator\"-Gruppe!";
		
		Operations.sendMessage(args, msg);
		if (quit) System.exit(0);
	}
}
