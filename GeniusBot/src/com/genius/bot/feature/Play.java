package com.genius.bot.feature;

import com.genius.bot.core.Operations;
import com.genius.bot.core.Slots;
import com.genius.bot.event.*;

public class Play implements ICommandListener {

	Slots slots = null;
	
	public Play(){
		slots = new Slots();
	}
	
	@Override
	public void onCommand(CommandEventArgs args) {
		slots = Operations.checkSlotsInstance(slots);
		String r = slots.pull();
		Operations.sendMessage(args, r);
	}
	
}