package com.genius.bot.feature;

import com.genius.bot.core.DbConnection;
import com.genius.bot.core.Operations;
import com.genius.bot.event.*;

public class Graph implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		String msg = null;
		String user = args.getParams().get(0);
		try {
			DbConnection dbcon = Operations.getDbConnection();
			msg = dbcon.getGraph(user);
			msg = msg != null ? msg : "Nutzer " + user + " nicht in der Datenbank vorhanden!\nBitte via https://larsbutnotleast.xyz/geniusgraph/ registrieren.";
			Operations.sendMessage(args, msg);
			}
		catch (Exception e) {
			e.printStackTrace();
			Operations.sendMessage(args, args.getIssuer().mention() + ": " + e.getMessage());
		}
	}
	
}