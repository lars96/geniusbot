package com.genius.bot.feature;

import com.genius.bot.core.Operations;
import com.genius.bot.event.CommandEventArgs;
import com.genius.bot.event.ICommandListener;
import com.vdurmont.emoji.EmojiManager;

import sx.blah.discord.util.RateLimitException;

public class Poll implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		try {
			args.getMessage().addReaction(EmojiManager.getForAlias("+1"));
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			args.getMessage().addReaction(EmojiManager.getForAlias("-1"));
		} catch(RateLimitException e) {
			Operations.sendMessage(args, args.getIssuer().mention() + ": Konnte Poll nicht erstellen. Fehler: RateLimitException");
			e.printStackTrace();
		}

	}

}
