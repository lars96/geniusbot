package com.genius.bot.feature;

import java.sql.SQLException;

import com.genius.bot.core.DbConnection;
import com.genius.bot.core.Operations;
import com.genius.bot.core.Privileges;
import com.genius.bot.event.*;

public class ListUsers implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		String msg = null;
		DbConnection dbcon = Operations.getDbConnection();
		if (Privileges.checkBotUserPrivileges(args.getIssuer().getRolesForGuild(args.getChannel().getGuild())))
		{
			try {
				msg = dbcon.getAllUsers();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else {
			msg = "Du bist nicht in der \"Bot User\"-Gruppe!";
		}
		Operations.sendMessage(args, args.getIssuer().mention() + ": " + msg);			
	}
	
}