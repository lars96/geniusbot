package com.genius.bot.feature;

import java.util.List;

import com.genius.bot.core.Operations;
import com.genius.bot.event.*;

import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.util.DiscordException;

public class ListenCmd implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		IVoiceChannel v = null;
		v = searchForMusicBot(args.getChannel().getGuild().getVoiceChannels());
		if (v != null) {
			if (!Operations.isUserInVoiceChannel(v, args.getIssuer().getLongID())) {
				try {
					args.getIssuer().moveToVoiceChannel(v);
					Operations.sendMessage(args, args.getIssuer().mention() 
							+ ", du wurdest in den Channel des Musik-Bots verschoben!");
	
				} catch (DiscordException e) {
					Operations.sendMessage(args, args.getIssuer().mention() 
							+ ", du musst bereits in einem Voice-Channel sein!");
				}
			}
			else {
				Operations.sendMessage(args, args.getIssuer().mention() 
						+ ", du befindest dich bereits im Voice-Channel des Musik-Bots!");
			}
		}
		else
		{
			Operations.sendMessage(args, args.getIssuer().mention() + ": Es ist kein Musik-Bot online!");
		}
	}
	
	private IVoiceChannel searchForMusicBot(List<IVoiceChannel> v) {
		for (IVoiceChannel voicechannel : v) {
			for (IUser u : voicechannel.getUsersHere()) {
				if (u.getLongID() == Operations.getOwnUser().getLongID()){
					return voicechannel;
				}
			}
		}
		return null;
	}
	
}