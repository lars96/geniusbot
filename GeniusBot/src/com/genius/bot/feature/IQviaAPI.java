package com.genius.bot.feature;

import com.genius.bot.core.APIConnection;
import com.genius.bot.core.DbConnection;
import com.genius.bot.core.Operations;
import com.genius.bot.event.*;
import com.google.gson.JsonObject;

public class IQviaAPI implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		String msg = null;
		if (args.getParams().size() == 1) {
			String user = args.getParams().get(0);
			DbConnection dbcon = Operations.getDbConnection();
			try {
				String token = dbcon.getAuthToken(user);
				if (token != null) {
					APIConnection api = new APIConnection(token);
					JsonObject response = api.getRequest("account");
					if (response != null ){
						String display_iq = response.getAsJsonObject("response").getAsJsonObject("user").getAsJsonPrimitive("iq_for_display").getAsString();
						msg = "IQ f�r den User " + user + " ist " + display_iq;		
					}
				}
				else {
					msg = "Nutzer " + user + " nicht in der Datenbank vorhanden!\nBitte via https://larsbutnotleast.xyz/geniusgraph/ registrieren.";
				}

				Operations.sendMessage(args, msg);	
			}
			catch (Exception e) {
				Operations.sendMessage(args, args.getIssuer().mention() + ": " + e.getMessage());
			}
		}
		
	}
	
}