package com.genius.bot.feature;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.Duration;
import java.time.ZonedDateTime;
import com.genius.bot.core.DbConnection;
import com.genius.bot.core.Operations;
import com.genius.bot.core.Privileges;
import com.genius.bot.event.*;

public class Status implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		if (Privileges.checkAdminPrivileges(args)) {
			sendStatus(args);
		}
		else {
			Operations.sendMessage(args, args.getMessage().getAuthor().mention() + ": Du bist nicht in der \"Administrator\"-Gruppe!");
		}
	}

	private void sendStatus(CommandEventArgs args) {
		
		DbConnection dbcon = Operations.getDbConnection();
		String regUsersCount = "";
		String fetchedIQCount = "";
		
		if (dbcon != null) {
			regUsersCount = String.valueOf(dbcon.countRegisteredUsers());
			fetchedIQCount = String.valueOf(dbcon.countFetchedIQs());
		}
		else
		{
			regUsersCount = fetchedIQCount = "Fehler!";
		}
		
		
		String stat = "```Status:\n\n";
		//Launched
		stat += "Gestartet: " + Operations.getFormattedLaunchTimestamp() + "\n";
		//Duration
		stat += "Ausgef�hrt f�r: " + FormatDuration() + "\n";
		//Server Runtime
		stat += "Server l�uft seit: " + getServerUptime() + "\n";
		//DB
		stat += "Datenbank: ";
		stat += Operations.getDbConnection() != null ? "OK\n" : "Nicht OK\n";
		//IP
		String ip = getIP();
		stat += "IP-Adresse: " + ip + "\n";
		//Modus
		stat += "Modus: ";
		stat += Operations.getUptime().equals("unknown") ? "Debug" : "Live";
		stat += "\n";
		//GeniusGraph
		stat += "**Genius Graph**\n";
		stat += "Registrierte Nutzer: " + regUsersCount + "\n";
		stat += "Bisher erfasste Datens�tze: " + fetchedIQCount + "\n";
		
		stat += "```";
		Operations.sendMessage(args, stat);
	}
	
	private String getIP() {
		BufferedReader in;
		try {
			URL checkip = new URL("http://checkip.amazonaws.com");
			in = new BufferedReader(new InputStreamReader(checkip.openStream()));
			String ip = in.readLine();
			return ip;
		} 
		catch (IOException e) {
			return "Fehler beim Lesen der IP-Adresse.";
		}
	}
	
	private String FormatDuration() {
		ZonedDateTime cur = ZonedDateTime.now();
		Duration dur = Duration.between(cur, Operations.getLaunchTimestamp());
		long s = dur.getSeconds();
		//TODO: Replace Math.abs()...
		return String.format("%d:%02d:%02d", Math.abs(s / 3600), Math.abs((s % 3600) / 60), Math.abs((s % 60)));		
	}
	
	private String getServerUptime() {
		String uptime = Operations.getUptime();
		if(uptime == "unknown") {
			return "In Debug-Mode nicht verf�gbar.";
		} else {
			return uptime.split(" ")[3] + " Tagen, " + uptime.split(" ")[1].split(":")[0] + " Stunden, " + uptime.split(" ")[1].split(":")[1] + " Minuten und " + uptime.split(" ")[1].split(":")[2] + " Sekunden";
		}
	}
	
}