package com.genius.bot.feature;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import com.genius.bot.event.CommandEventArgs;
import com.genius.bot.event.ICommandListener;

public class Unshorten implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		if(args.getParams().size() == 1) {
			String url = args.getParams().get(0);
			String newurl = url;
			try {
				URL jvmurl  = new URL(url);  
		        HttpURLConnection urlconn = (HttpURLConnection) jvmurl.openConnection(); 
		        urlconn.setInstanceFollowRedirects(false);
		        newurl = urlconn.getHeaderField("Location");
		        urlconn.disconnect();
		        if(newurl == null) {
		        	args.getChannel().sendMessage(args.getIssuer().mention() + ": Konnte keine Verbindung zur URL herstellen!");
		        } else {
		        	args.getChannel().sendMessage("Unshortened URL: " + newurl);
		        }
			} catch(MalformedURLException ex) {
				args.getChannel().sendMessage(args.getIssuer().mention() + ": Ung�ltige URL!");
			} catch (IOException ex) {
				args.getChannel().sendMessage(args.getIssuer().mention() + ": Konnte keine Verbindung zur URL herstellen!");
				ex.printStackTrace();
			}
		} else {
			args.getChannel().sendMessage(args.getIssuer().mention() + ": Ung�ltige Parameter!");
		}
	}

}
