package com.genius.bot.feature;

import java.util.List;
import com.genius.bot.core.Operations;
import com.genius.bot.event.*;
import at.mukprojects.giphy4j.*;
import at.mukprojects.giphy4j.entity.search.SearchRandom;
import at.mukprojects.giphy4j.exception.GiphyException;

public class GiphyCmd implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		Giphy giphy = new Giphy("dc6zaTOxFJmzC");
		SearchRandom giphyData;
		List<String> arguments = args.getParams();
		String text = "";
		for(String i : arguments) {
			text += i + " ";
		}
		text = text.substring(0, text.length() - 1);
		args.getMessage().delete();
		try {
			giphyData = giphy.searchRandom(text);
			String str = giphyData.getData().getImageOriginalUrl();
			Operations.sendMessage(args, args.getIssuer().mention() + ": \"" + text + "\"\n" + str);
		} catch (GiphyException e) {
			e.printStackTrace();
			Operations.sendMessage(args, args.getIssuer().mention() + ": Sorry, es konnte kein passendes Bild f�r \"" + text + "\" gefunden werden!");
		}
	}
}