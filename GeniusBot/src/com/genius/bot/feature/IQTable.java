package com.genius.bot.feature;

import java.util.List;

import com.genius.bot.core.DbConnection;
import com.genius.bot.core.IQTableUser;
import com.genius.bot.core.Operations;
import com.genius.bot.event.*;

public class IQTable implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		String msg = "";
		List<IQTableUser> table = null;
		if (args.getParams().size() == 1) {
			String user = args.getParams().get(0);
//			msg += user + ":\n\n";
			try{
				DbConnection dbcon =  Operations.getDbConnection();
				table =  dbcon.getTabledData(user);
				if (table == null){
					msg = "Nutzer " + user + " nicht in der Datenbank vorhanden!\nBitte via https://larsbutnotleast.xyz/geniusgraph/ registrieren.";
				}
				else {
					int length_of_IQ_String = table.get(0).getIQ().length();
					
					msg += "```";
					msg += "Letzten " + table.size() + " tausender IQ-Werte f�r ";
                    msg += user + ":\n\n";
					for (IQTableUser tableUser : table) {
						int cur_len = tableUser.getIQ().length();
						if (cur_len < length_of_IQ_String) {
							int spaces_to_be_inserted = length_of_IQ_String - tableUser.getIQ().length();
							for (int i = 0; i < spaces_to_be_inserted; i++) {
								msg += " ";
							}
						}
						
						msg += tableUser.getIQ() + "\t\t" + tableUser.getDate() + "\n";
					}
					msg += "```";
				}
				Operations.sendMessage(args, msg);
			}
			catch (Exception e) {
				Operations.sendMessage(args, args.getIssuer().mention() + ": " + e.getMessage());
				e.printStackTrace();
			}
		}
	}
}