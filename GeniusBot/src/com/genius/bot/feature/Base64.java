package com.genius.bot.feature;

import com.genius.bot.event.CommandEventArgs;
import com.genius.bot.event.ICommandListener;
import javax.xml.bind.DatatypeConverter;

public class Base64 implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
		if(args.getParams().size() >= 2) {
			if(args.getParams().get(0).equals("encode")) {
				try {
					String plaintext;
					if(args.getParams().size() == 2) {
						plaintext = args.getParams().get(1);
					} else {
						int index = 0;
						plaintext = args.getParams().get(1);
						for(String i : args.getParams()) {
							if(!(index == 0) && (!(index == 1))) {
								plaintext += " " + i;
							}
							index++;
						}
					}
					String encodedtext = DatatypeConverter.printBase64Binary(plaintext.getBytes());
					args.getChannel().sendMessage("```" + plaintext + "``````" + encodedtext + "```");
				} catch(Exception ex) {
					args.getChannel().sendMessage(args.getIssuer().mention() + ": Konnte Text nicht umwandeln!");
				}
			} else if(args.getParams().get(0).equals("decode")) {
				try {
					String encodedtext;
					if(args.getParams().size() == 2) {
						encodedtext = args.getParams().get(1);
					} else {
						int index = 0;
						encodedtext = args.getParams().get(1);
						for(String i : args.getParams()) {
							if(!(index == 0) && (!(index == 1))) {
								encodedtext += " " + i;
							}
							index++;
						}
					}
					String plaintext = new String(DatatypeConverter.parseBase64Binary(encodedtext));
					args.getChannel().sendMessage("```" + encodedtext + "``````" + plaintext + "```");
				} catch(Exception ex) {
					args.getChannel().sendMessage(args.getIssuer().mention() + ": Konnte Text nicht umwandeln!");
				}
			} else {
				args.getChannel().sendMessage(args.getIssuer().mention() + ": Ung�ltige Parameter!");
			}
		} else {
			args.getChannel().sendMessage(args.getIssuer().mention() + ": Ung�ltige Parameter!");
		}
	}

}
