package com.genius.bot.feature;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import com.genius.bot.core.DbConnection;
import com.genius.bot.core.IQTableUser;
import com.genius.bot.core.Operations;
import com.genius.bot.event.*;

public class WhenIQ implements ICommandListener {

	@Override
	public void onCommand(CommandEventArgs args) {
//		if (Privileges.checkBotUserPrivileges(args))
//		{
			if (args.getParams().size() == 2) {
				String user = args.getParams().get(0);
				Long forecastIQ = Long.parseLong(args.getParams().get(1));
				try {
					DbConnection dbcon = Operations.getDbConnection();
					List<IQTableUser> li = dbcon.getEntireIQ(user);
					int count = li.size();
					

					
					if (count > 0) {
						long first = Long.parseLong(li.get(0).getIQ());
						long last = Long.parseLong(li.get(count - 1).getIQ());
						
						long diff = last - first;
						long perday = diff / count;
						
						if (perday == 0) {
							Operations.sendMessage(args, "Nicht gen�gend Daten vorhanden!");
							return;
						}
						
						long iq2make = forecastIQ - last;
						long remainding = (long) Math.ceil(iq2make / perday);
						
						ZonedDateTime cur = ZonedDateTime.now();
						ZonedDateTime fut = cur.plusDays(remainding);
						
//						DateTimeFormatter dtf = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
						String ret = args.getIssuer().mention() + ": Der Nutzer " + user;
						ret += remainding > 0 ? " wird " + forecastIQ + " IQ rechnerisch erreichen am " : " erreichte " + forecastIQ + " IQ rechnerisch am ";

//						System.out.println(DateTimeFormatter.ofPattern("dd/MM/yyyy - hh:mm").format(fut));
						System.out.println(DateTimeFormatter.ofPattern("dd. LLL yyyy").format(fut));
						
//						ret += fut.format(dtf);
						ret += DateTimeFormatter.ofPattern("dd. LLL yyyy").format(fut);
						Operations.sendMessage(args, ret);
					}
				}
				catch (Exception e) {
					System.out.println(e.getMessage());
				}
//			}
		}
	}
}
	
