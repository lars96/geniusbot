package com.genius.bot.core;

import java.util.HashMap;
import java.util.HashSet;
import com.genius.bot.event.CommandNotifier;
import com.genius.bot.event.ICommandListener;

import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

public class Command extends CommandNotifier {

	private String command;
	private String description;
	private String help = "(No help available)";
	private HashMap<String, HashSet<String>> servers = new HashMap<>();

	public Command(String command, String description, ICommandListener listener) {
		this.command = command;
		this.description = description;
		addListener(listener);
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command.trim().toLowerCase();
	}

	public String getDescription() {
		return description;
	}

	public String getHelp() {
		return help;
	}

	public void setHelp(String help) {
		if (help == null)
			help = "";
		this.help = help;
	}

	public String generateHelp(Commands commands, IGuild guild) {
		StringBuilder sb = new StringBuilder("```");
		sb.append(commands.getDelimiter());
		sb.append(command);
		sb.append("\n\t");
		sb.append(description);
		sb.append("\n\n\t");
		sb.append(help.replaceAll("\\$CMD\\$", command));
		if (guild != null) {
			sb.append("\n\n\tRequired roles:");
			String gid = guild.getStringID();
			if (servers.containsKey(gid)) {
				HashSet<String> roles = servers.get(gid);
				boolean hn = true;
				for (String rk : roles) {
					IRole role = guild.getRoleByID(Long.parseLong(rk));
					if (role != null) {
						sb.append("\n\t\t");
						sb.append(role.getName());
						sb.append(" : ");
						sb.append(role.getStringID());
						hn = false;
					}
				}
				if (hn)
					sb.append(" None");
			} else
				sb.append(" None");
		}
		sb.append("\n\n\tRequired privileges:");
		sb.append(" None");
		sb.append("```");
		return sb.toString();
	}

	public boolean checkRoles(IGuild guild, IUser user) {
		boolean ret = true;
		String gid = guild.getStringID();
		if (servers.containsKey(gid)) {
			for (String role : servers.get(gid)) {
				IRole r = guild.getRoleByID(Long.parseLong(role));
				if (r != null) {
					ret = false;
					for (IRole ur : user.getRolesForGuild(guild)) {
						if (ur.getStringID() == role) {
							ret = true;
							break;
						}
					}
				}
			}
		}
		return ret;
	}

	public void addRoleForServer(IGuild guild, IRole role) {
		String gid = guild.getStringID();
		HashSet<String> roles = null;
		if (servers.containsKey(gid))
			roles = servers.get(gid);
		else
			roles = new HashSet<>();
		roles.add(role.getStringID());
		servers.put(gid, roles);
	}

	public void removeRoleForServer(IGuild guild, IRole role) {
		String gid = guild.getStringID();
		HashSet<String> roles = null;
		if (servers.containsKey(gid))
			roles = servers.get(gid);
		else
			roles = new HashSet<>();
		String rid = role.getStringID();
		if (roles.contains(rid))
			roles.remove(rid);
		servers.put(gid, roles);
	}
}
