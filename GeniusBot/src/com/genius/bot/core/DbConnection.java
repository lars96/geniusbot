package com.genius.bot.core;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import sx.blah.discord.handle.obj.IUser;

public class DbConnection {

	private Connection connection;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private boolean succeeded = false;
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
    static final String DB_URL = "jdbc:mysql://localhost:3306/geniusdb?autoReconnect=true&useUnicode=yes";
    static final String USER = "root";
    static final String PASS = Main.dbpass; 
	static Date connectionTimestamp = null;
	
    
	public DbConnection() {
		succeeded = connectToMysql();
	}

	public boolean connectToMysql(){
		try{
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			String passS;
			if(PASS.equals("null")) {
				passS = null;
			} else {
				passS = PASS;
			}
			connection = DriverManager.getConnection(DB_URL,USER,passS);
			return true;
		
		}catch (Exception ex){
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ((SQLException) ex).getSQLState());
		    System.out.println("VendorError: " + ((SQLException) ex).getErrorCode());
			return false;
		}
	}
	public String getIQ(String username) throws SQLException{
		String ret = null;
		try{
			String id = Operations.getGeniusID(username);
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT displaytotalIQ FROM iqtrend WHERE username = '" + id + "' ORDER BY fetchdatetime DESC LIMIT 1");
		    if (resultSet.next()){
	        	ret =  resultSet.getString("displaytotalIQ");
	        	}
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
		}
		return ret;
	}
	
	public List<IQTableUser> getEntireIQ(String username) {
		List<IQTableUser> li = new ArrayList<IQTableUser>();
		String id = Operations.getGeniusID(username);
		try{
			statement = connection.createStatement();
			resultSet = statement.executeQuery("SELECT totaliq, fetchdatetime FROM iqtrend WHERE username = '" + id + "'");
		    while (resultSet.next()){
		    	li.add(new IQTableUser(resultSet.getString(1), resultSet.getString(2)));
		    }
		}
		catch (SQLException e){
			System.out.println(e.getMessage());
		}
		return li;
	}
	
	public String getGraph(String username) throws SQLException {
		String ret = null;
		String gid = Operations.getGeniusID(username);
		statement = connection.createStatement();
		String query = "SELECT userid FROM graph WHERE id = '" + gid + "'";
		resultSet = statement.executeQuery(query);
	    if (resultSet.next()){
	    	String id = resultSet.getString("userid");
	    	ret = "https://larsbutnotleast.xyz/geniusgraph/users/" + id + "/latest.jpg";
	    }
		return ret;
	}
	
	public String getGeniusID(String username) throws Exception {
		statement = connection.createStatement();
		String query = "SELECT id FROM graph WHERE username = '" + username + "'";
		resultSet = statement.executeQuery(query);
	    if (resultSet.next()){
	    	String id = resultSet.getString("id");
	    	return id;
	    }
		return "";
	}
	
	public String getAuthToken(String username) throws Exception {
		String ret = null;
//		String id = Operations.getGeniusID(username);
		statement = connection.createStatement();
		String query = "SELECT token FROM graph WHERE username = '" + username + "'";
		resultSet = statement.executeQuery(query);
	    if (resultSet.next()){
	    	ret = resultSet.getString("token");
	    }
		return ret;
	}
	
	public String getAllUsers() throws SQLException {
		ArrayList<String> li = new ArrayList<String>();
		
		int count = 0;
		String ret = "";
		String ret2 = "";
		statement = connection.createStatement();
		String query = "SELECT username FROM graph WHERE username != '' AND active = 1 AND username != '???' ";
		resultSet = statement.executeQuery(query);
	    while (resultSet.next()){
	    	count++;
	    	li.add(resultSet.getString("username"));
	    }
	    
	    Collections.sort(li, new Comparator<String>() {
	        @Override
	        public int compare(String s1, String s2) {
	            return s1.compareToIgnoreCase(s2);
	        }
	    });

	    for (int i = 0; i < li.size(); i++) {
    		String usr = li.get(i);
    		ret += usr + "\t";
    	}
	    
	    ret2 = " Alle registrierten User (" + count + "):\n" + ret;
        return ret2;
    }
	
	public long countRegisteredUsers() {
		String ret = null;
		try {
			statement = connection.createStatement();
			String query = "SELECT COUNT(*) FROM graph WHERE active = 1";
			resultSet = statement.executeQuery(query);
		    if (resultSet.next()){
		    	ret = resultSet.getString(1);
		    }
			return Long.parseLong(ret);
		}
		catch (SQLException e) {
			return 0;
		}
	}
	
	public long countFetchedIQs() {
		String ret = null;
		try {
			statement = connection.createStatement();
			String query = "SELECT COUNT(*) FROM iqtrend";
			resultSet = statement.executeQuery(query);
		    if (resultSet.next()){
		    	ret = resultSet.getString(1);
		    }
			return Long.parseLong(ret);
		}
		catch (SQLException e) {
			return 0;
		}
	}
	
	public List<IQLeaderboardUser> getLeaderboardsGraph(){
		List<IQLeaderboardUser> li = new ArrayList<IQLeaderboardUser>();
		try {
			statement = connection.createStatement();
			String query = "SELECT username, totalIQ As aktuell, (SELECT totalIQ FROM iqtrend WHERE username = iq.username AND DATE(fetchdatetime) = subdate(curdate(), 1) LIMIT 1) AS gestern, totalIQ - (SELECT totalIQ FROM iqtrend WHERE username = iq.username AND DATE(fetchdatetime) = subdate(curdate(), 1) LIMIT 1) AS Differenz FROM iqtrend AS iq WHERE DATE(fetchdatetime) = curdate() ORDER BY Differenz DESC LIMIT 10;";
			
			resultSet = statement.executeQuery(query);
		    while (resultSet.next()){
		    	li.add(new IQLeaderboardUser(resultSet.getString(1), resultSet.getString(2)));
		    }
		} catch (SQLException e) {
			return null;
		}
		return li;
	}
	
	public List<IQTableUser> getTabledData(String user) {
		List<IQTableUser> li = new ArrayList<IQTableUser>();
		String id = Operations.getGeniusID(user);
		try {
			statement = connection.createStatement();
			String query = "SELECT TRUNCATE((totaliq / 1000), 0) * 1000, DATE(fetchdatetime), totaliq FROM `iqtrend` WHERE `username` = \"" + id + "\" GROUP BY (TRUNCATE((totaliq / 1000), 0)) * 1000 ORDER BY fetchdatetime DESC LIMIT 15;";
			resultSet = statement.executeQuery(query);
		    while (resultSet.next()){
		    	String formattedIQ = "";
		    	String formattedDate = "";
		    	//IQ
		    	int iq = resultSet.getInt(1);
		    	formattedIQ = String.format("%,d", iq);
		    	//Date
		    	Date d = resultSet.getDate(2);
		    	formattedDate = Operations.parseDate(d);
		    	
		    	li.add(new IQTableUser(formattedIQ, formattedDate));
		    }
		} catch (SQLException e) {
			return null;
		}
		return li;
	}
	
	public String getSettingValue(int i) {
		String ret = null;
		try {
			statement = connection.createStatement();
			String query = "SELECT val FROM settings WHERE idx = " + i;
			resultSet = statement.executeQuery(query);
		    if (resultSet.next()){
		    	ret = resultSet.getString(1);
		    }
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	public boolean checkGeniusUserPermission(IUser discorduser, String geniususer) {
		if (RoleChecker.check(discorduser, Constants.getRoleID.ADMIN.toString())) return true;
		
		try {
			statement = connection.createStatement();
			String query = "SELECT id FROM privileges WHERE discordID = '" + String.valueOf(discorduser.getLongID()) + "' and GeniusUsername = '" + geniususer + "'";
			resultSet = statement.executeQuery(query);
		    return resultSet.next() ? true : false;
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void insertGeniusUserPermission(String discorduser, String geniususer) {
		try {
			statement = connection.createStatement();
			String query = "INSERT INTO privileges(discordID, GeniusUsername) VALUES ('" + discorduser + "', '" + geniususer + "')";
			statement.execute(query);
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}	
	}

	public int getTag(String description) throws Exception {
		int ret = 0;
		statement = connection.createStatement();
		String query = "SELECT tag FROM tags WHERE description = '" + description + "'";
		resultSet = statement.executeQuery(query);
	    if (resultSet.next()){
	    	ret = resultSet.getInt("tag");
	    }
		return ret;
	}
	
	//TODO: Create class: Tag with properties tag, description, longDescription
	public String getTagDescription(int tag) throws Exception {
		String ret = null;;
		statement = connection.createStatement();
		String query = "SELECT longDescription FROM tags WHERE tag = " + tag;
		resultSet = statement.executeQuery(query);
	    if (resultSet.next()){
	    	ret = resultSet.getString("longDescription");
	    }
		return ret;
	}
	
    public boolean isSucceeded() {
		return succeeded;
	}

    public boolean isClosed(){
		try {
			if (connection != null) {
				return connection.isClosed();
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return true;
		}
	}
}
