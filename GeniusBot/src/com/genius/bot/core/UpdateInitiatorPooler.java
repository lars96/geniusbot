package com.genius.bot.core;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class UpdateInitiatorPooler extends Thread {

	public void run() {
		
		try {	
			while (true) {
				Path filePath = Paths.get("/home/lars/Skripts/geniusbot/", Constants.getFileName.UPDATE_INITIATOR_FILE.toString());
				File f = filePath.toFile();
				if (Operations.getOwnGuild() != null && f.exists()) {
					f.delete();
					Operations.sendMessage(Operations.getGeneralChannel(),
							"Ein Update wurde initiiert. Der Bot wurde erfolgreich beendet - bis gleich :wave:");
					System.exit(Constants.getShutdownCode.CODE_MAINTAINANCE.toInt());
				}
				sleep(5000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
