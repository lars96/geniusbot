package com.genius.bot.core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import com.genius.bot.event.CommandEventArgs;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelLeaveEvent;
import sx.blah.discord.handle.impl.events.user.PresenceUpdateEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

public class Operations {

	private static boolean isRateLimited = false;
	private static boolean use_tts = false;
	private static DbConnection dbcon = null;
	private static ZonedDateTime ldt = null;
	private static IGuild geniusDeuGuild = null;
	private static IUser bothimself = null;
	private static final int MAX_UNREAD_CHARACTERS = 40;
	
	public static void sendMessage(IChannel channel, String text) {
		try {
			if (!isRateLimited) channel.sendMessage(text, use_tts);
		} catch (MissingPermissionsException e) {
			e.printStackTrace();
		} catch (DiscordException e) {
			e.printStackTrace();
		} catch (RateLimitException e) {
			e.printStackTrace();
			try {
				isRateLimited = true;
				long de = e.getRetryDelay();
				Thread.sleep(de + 10);
				isRateLimited = false;
				sendMessage(channel, "You were rate limted! Time limit was " + de + " milliseconds");
			}
			catch (InterruptedException ie){
				
			}
		}
	}	

	public static void sendMessage(CommandEventArgs args, String text) {
		sendMessage(args.getChannel(), text);
	}
	
	public static void initLaunchTimestamp()  {
		ldt = ZonedDateTime.now();
	}
	
	public static void setOwnGuild(IGuild guild) {
		geniusDeuGuild = guild;
	}
	
	public static IGuild getOwnGuild() {
		return geniusDeuGuild;
	}
	
	public static void setOwnUser(IUser user) {
		bothimself = user;
	}
	
	public static IUser getOwnUser() {
		return bothimself;
	}
	
	public static ZonedDateTime	getLaunchTimestamp() {
		return ldt;
	}
	
	public static IChannel getGeneralChannel() {
		return geniusDeuGuild.getChannelByID(Long.parseLong(Constants.getChannelID.GENERAL_CHANNEL.toString()));
	}
	
	public static IRole getGuestRole() {
		return geniusDeuGuild.getRoleByID(Long.parseLong(Constants.getRoleID.GUEST.toString()));
	}
	
	public static String getFormattedLaunchTimestamp() {
		DateTimeFormatter dtf = DateTimeFormatter.RFC_1123_DATE_TIME;
		return dtf.format(ldt);
	}
	
	public static String formatIQ(double iq){
		String s;
		if ((iq - (long)iq) == 0){
			s = String.valueOf((int)iq);
		}
		else{
			s = String.valueOf(iq);
		}
		return s;
	}
	
	public static DbConnection getDbConnection(){
		boolean createNewDbConnection = true;
		if (dbcon != null){
			createNewDbConnection = dbcon.isClosed() ? true : false;
		}
		
		//Current DbConnection is closed or null => new DbConnection
		if (createNewDbConnection) {
			dbcon = new DbConnection();
		}
		
		//If login was not successful return null
		return dbcon.isSucceeded() ? dbcon : null;
	}
	
	public static String getGeniusID(String username) {
		DbConnection dbcon = getDbConnection();
		try {
			return dbcon.getGeniusID(username);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Slots checkSlotsInstance(Slots s){
		return s != null ? s : new Slots();
	}

	public static String getUptime() {
		Process p;
		String output = "";
		try {
			p = Runtime.getRuntime().exec("uptime");
			p.waitFor();
			BufferedReader buf = new BufferedReader(new InputStreamReader(
			p.getInputStream()));
			String line = "";
			while ((line = buf.readLine()) != null) {
			  output += line;
			}
			buf.close();
			p.destroy();
		} catch (IOException e) {
			// e.printStackTrace();
			output = "unknown";
		} catch (InterruptedException e) {
			e.printStackTrace();
			// set output to prevent client from crashing
			output = "unknown";
		}
		return output;
	}

	
    public static String getImgurContent(String q) throws Exception {
        String qurey = q;
        qurey = qurey.replaceAll(" ", "+");
        URL url;
        url = new URL("https://api.imgur.com/3/gallery/search/hot/all/0/?q=" + qurey);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();

        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Authorization", "Client-ID " + "0d6fe9cb2fca42c");

        conn.connect();
        StringBuilder stb = new StringBuilder();

        // Get the response
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            stb.append(line).append("\n");
        }
        rd.close();
        String split = stb.toString();
        split = split.replaceAll("[^A-Za-z0-9 ]", " ");
        String ID;
        String baseURL = "http://i.imgur.com/";
        List<String> items = Arrays.asList(split.split("\\s+"));
        ID = items.get(items.indexOf("id") + 1);
        String adrs = baseURL + ID;
        if (adrs.equalsIgnoreCase(baseURL) || ID == null) {
            return "No image found";
        }
        return adrs;
    }
    
    public static String deleteMessages(List<String> li, CommandEventArgs args, int count) {
    	int c = 0;
    	String ret = "";
    	for (String s : li){
    		c++;
    		if (c > count) return String.valueOf(c) + " Nachrichten wurden gel�scht.\n";
    		try {
				args.getChannel().getMessageByID(Long.parseLong(s)).delete();
			} catch (MissingPermissionsException e) {
				e.printStackTrace();
			} catch (RateLimitException e) {
				e.printStackTrace();
				ret += "RateLimited nach " + c + " Nachrichten";
			} catch (DiscordException e) {
				e.printStackTrace();
			}
    	}
		return ret;
    }

    public static String convertToEmojiNumber(String count) {
    	String ret = "";
    	String text = count;
		
		int len = text.length() - 1;
		for (int i = 0; i < len; i++) {
			char c = text.charAt(i);
			ret += ":" + c + "_:";
		}
    	return ret;
    }
    public static String convertToEmojiNumber(int count) {
    	String ret = "";
    	String text = String.valueOf(count);
		
		int len = text.length();
		for (int i = 0; i < len; i++) {
			char c = text.charAt(i);
			ret += ":" + c + "_:";
		}
    	return ret;
    }    
    public static String parseDate(Date d)  {
    	SimpleDateFormat sd = new SimpleDateFormat("dd. MMMM yyyy");
    	String dateString = sd.format(d); 
    	return dateString;
    }
    
    public static void autoAssignRole(PresenceUpdateEvent userJoined) {
    	int roleCount = userJoined.getUser().getRolesForGuild(geniusDeuGuild).size();
    	IUser user = userJoined.getUser();
    	IRole generalRole = geniusDeuGuild.getRoleByID(Long.parseLong(Constants.getRoleID.GUEST.toString()));
    	
//    	RoleCount > 1 = Role assigned; RoleCont == 1 = only @everyone
    	if (roleCount == 1) {
    		try {
				user.addRole(generalRole);
//				Send message to #g channel
				Operations.sendMessage(geniusDeuGuild.getChannelByID(Long.parseLong(Constants.getChannelID.WELCOME_CHANNEL.toString())), "Willkommen auf dem Genius Deutschland Discord-Server, " + user.mention() + "!");
			} catch (MissingPermissionsException e) {
				e.printStackTrace();
			} catch (RateLimitException e) {
				e.printStackTrace();
			} catch (DiscordException e) {
				e.printStackTrace();
			}
    	}
    }
    public static boolean isUserInVoiceChannel(IVoiceChannel voicechannel, long userID) {
    	for (IUser user : voicechannel.getUsersHere()) {
    		if (user.getLongID() == userID) {
    			return true;
    		}
    	}
    	return false;
    }
    
    public static String formatUnread(String desc, long count){
    	int desc_length = desc.length();
    	int count_length = String.valueOf(count).length();
    	int spaces_count = MAX_UNREAD_CHARACTERS - desc_length - count_length;
    	
    	String str = desc;
    	for (int i = 1; i < spaces_count; i++) {
    		str += " ";
    	}
    	str += String.valueOf(count);
    	return str;
    }
    
    public static String formatLeaderboards(String user, String iq) {
    	int user_length = user.length();
    	int iq_length = String.valueOf(iq).length();
    	int spaces_count = MAX_UNREAD_CHARACTERS - user_length - iq_length;
    	
    	String str = user;
    	for (int i = 1; i < spaces_count; i++) {
    		str += " ";
    	}
    	str += String.valueOf(iq);
    	return str;
    }
	public static String escapeUnicode(String input) {
		StringBuilder b = new StringBuilder(input.length());
		Formatter f = new Formatter(b);
		for (char c : input.toCharArray()) {
			if (c < 128) {
				b.append(c);
		    } else {
		    	f.format("\\u%04x", (int) c);
		    }
		}
		f.close();
		return b.toString();
	}
	public static String escapeNonPrintableIcons(String input) {
		StringBuilder b = new StringBuilder(input.length());
		Formatter f = new Formatter(b);
		for (char c : input.toCharArray()) {
			if (c < 128) {
				b.append(c);
		    }
		}
		f.close();
		return b.toString();
	}
	

	
	public static String stripOffNonPrintableCharacters(String s) {
		return s.replaceAll("\u200b", "")
				.replaceAll("\u00e2", "")
				.replaceAll("\u20ac", "")
				.replaceAll("\u2039", "");
	}
	
	public static void handleVoiceChatEvent(Object args) {
		if (args.getClass() == sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelJoinEvent.class) {
			sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelJoinEvent arg = (sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelJoinEvent) args;
			Operations.sendMessage(Operations.getOwnGuild().getChannelByID(Long.parseLong(Constants.getChannelID.VOICE_LOG_CHANNEL.toString())),
					arg.getUser().mention() + " :arrow_forward: `" + Operations.escapeNonPrintableIcons(arg.getVoiceChannel().getName()) + "`.");
		}
		else if (args.getClass() == UserVoiceChannelLeaveEvent.class) {
			sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelLeaveEvent arg = (sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelLeaveEvent) args;
			Operations.sendMessage(Operations.getOwnGuild().getChannelByID(Long.parseLong(Constants.getChannelID.VOICE_LOG_CHANNEL.toString())),
					arg.getUser().mention() + " :arrow_backward: `" + Operations.escapeNonPrintableIcons(arg.getVoiceChannel().getName()) + "`.");
		}
		else {
			sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelMoveEvent arg = (sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelMoveEvent) args;
			Operations.sendMessage(Operations.getOwnGuild().getChannelByID(Long.parseLong(Constants.getChannelID.VOICE_LOG_CHANNEL.toString())),
					arg.getUser().mention() + " `" + Operations.escapeNonPrintableIcons(arg.getOldChannel().getName()) + "` :twisted_rightwards_arrows: `" + Operations.escapeNonPrintableIcons(arg.getNewChannel().getName()) + "`.");
		}
	}
}


