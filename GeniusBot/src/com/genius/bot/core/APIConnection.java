package com.genius.bot.core;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class APIConnection {
	
	String token = null;
	
	public APIConnection() {
		token = Main.geniustok;
	}
	
	public APIConnection(String token) {
		this.token = token;
	}
	
	public JsonObject getRequest(String endpoint) throws Exception {
		String url = "https://api.genius.com/" + endpoint;

		JsonObject ret = null;
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", "CompuServe Classic/1.22");
		con.setRequestProperty("Authorization", "Bearer " + token);
		con.setRequestProperty("Accept", "application/json");

		int responseCode = con.getResponseCode();
		if (responseCode == 200) {
			JsonParser jp = new JsonParser();
		    JsonElement root = jp.parse(new InputStreamReader(con.getInputStream()));
		    ret = root.getAsJsonObject();
		}
	    return ret;
	}

}