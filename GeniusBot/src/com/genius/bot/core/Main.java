package com.genius.bot.core;


public class Main implements Runnable {

	private String token;
	public static String geniustok;
	public static String dbpass;
	public static char commanddelimiter;
	public Main(String token, String geniustok, String dbpass, char commanddelimiter) {
		this.token = token;
		Main.geniustok = geniustok;
		Main.dbpass = dbpass;
		Main.commanddelimiter = commanddelimiter;
	}

	public static void main(String[] args) {
		String token;
		String geniustok;
		String dbpass;
		char cmddelimiter;
		switch(args.length) {
		default:
			System.out.println("Invalid parameters! Syntax: <arg1:discordtoken> <arg2:geniustoken> <arg3:dbpass> <arg4:commanddelimiter>");
			System.exit(-1);
			break;	
		case(3):
			token = args[0];
			geniustok = args[1];
			dbpass = args[2];
			cmddelimiter = '!';
			Main bt = new Main(token, geniustok, dbpass, cmddelimiter);
			Thread t = new Thread(bt);
			t.start();
			break;
		case(4):
			token = args[0];
			geniustok = args[1];
			dbpass = args[2];
			cmddelimiter = args[3].charAt(0);
			Main bt1 = new Main(token, geniustok, dbpass, cmddelimiter);
			Thread t1 = new Thread(bt1);
			t1.start();
			break;
		}
		UpdateInitiatorPooler p = new UpdateInitiatorPooler();
		p.run();
		
	}

	@Override
	public void run() {
		Bot.login(token);
	}
}
