package com.genius.bot.core;

public class Constants {
	
	public static enum getRoleID {
		BOT_USER("296388902481166340"), ADMIN("295645828079419393"), GUEST("308678122184638464"), EDITOR("295645664896090113");
		
		private final String text;

	    private getRoleID(final String text) {
	        this.text = text;
	    }
	    
	    @Override
	    public String toString() {
	        return text;
	    }
	}
	
	public static enum getFileName {
		UPDATE_INITIATOR_FILE("update.me");
		
		private final String text;

	    private getFileName(final String text) {
	        this.text = text;
	    }
	    
	    @Override
	    public String toString() {
	        return text;
	    }
	}
	
	public static enum getChannelID {
		WELCOME_CHANNEL("285822817155678208"), GENERAL_CHANNEL("319802683521236992"), BOT_USAGE_CHANNEL("297073378433695744"), VOICE_LOG_CHANNEL("323164865424523294");
		
		private final String text;

	    private getChannelID(final String text) {
	        this.text = text;
	    }
	    
	    @Override
	    public String toString() {
	        return text;
	    }
	}
	

	public static enum getShutdownCode {
		CODE_REQUESTED(1), CODE_MAINTAINANCE(2);
		
		private final int code;
		
		private getShutdownCode(final int code) {
			this.code = code;
		}
		
		public int toInt() {
			return code;
		}
	}
}
