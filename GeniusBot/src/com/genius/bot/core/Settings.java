package com.genius.bot.core;

public  class Settings {

	public static final int DISCORD_TOKEN = 0;
	public static final int GENIUS_TOKEN = 1;
	public static final int DELIMITER = 2;
	
	public static final int MAX_SETTING = 2;
	public static String[] settings; 
	private static boolean readDone = false;
	
	public static void readSettings() {
		settings = new String[MAX_SETTING + 1];
		
		DbConnection dbcon = Operations.getDbConnection();
		for (int i = 0; i <= MAX_SETTING; i++) {
			settings[i] = dbcon.getSettingValue(i);
		}
	}
		
	private static void checkRead() {
		if (!readDone) {
			readDone = true;
			readSettings();
		}
	}
	
	public static String getDiscordAPIToken() {
		checkRead();
		return settings[DISCORD_TOKEN];
	}
	
	public static String getGeniusToken() {
		checkRead();
		return settings[GENIUS_TOKEN];
	}
	
	public static String getDelimiter() {
		checkRead();
		return settings[DELIMITER];
	}
}
