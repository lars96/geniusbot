package com.genius.bot.core;

import java.util.ArrayList;
import java.util.List;
import com.genius.bot.event.CommandEventArgs;

import sx.blah.discord.handle.impl.obj.User;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

public class RoleChecker {
	
	@SuppressWarnings("unchecked")
	public static boolean check(Object o, String role){
		List<IRole> li = new ArrayList<IRole>();
		System.out.println(o.getClass());
		
		if (o.getClass() == ArrayList.class ) {
			li = (List<IRole>) o;
		}
		else
		{
			if (o.getClass() == User.class) {
				User u = (User) o;
				li = u.getRolesForGuild(Operations.getOwnGuild());
			}
			else
			{
				if (o.getClass() == CommandEventArgs.class) {
					CommandEventArgs args = (CommandEventArgs) o;
					li = args.getIssuer().getRolesForGuild(Operations.getOwnGuild());
				}
				else
				{
//					throw new InvalidClassException(o.getClass());
				}
			}
			
		}
		
		for(IRole i : li){
			if (i.getStringID().equals(role)) {
				return true;
			}
		}
		return false;
		
	}
	
	public static boolean checkBotUserPrivileges(CommandEventArgs e){
		List<IRole> li = e.getIssuer().getRolesForGuild(e.getChannel().getGuild());
		return checkBotUserPrivileges(li);
	}

	public static boolean checkBotUserPrivileges(List<IRole> li){
		for(IRole i : li){
			if (i.getStringID().equals(Constants.getRoleID.BOT_USER.toString())) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean checkAdminPrivileges(CommandEventArgs e){
		return checkAdminPrivileges(e.getIssuer());
	}
	
	public static boolean checkAdminPrivileges(IUser user) {
		List<IRole> li = user.getRolesForGuild(Operations.getOwnGuild());
		for(IRole i : li){
			if (i.getStringID().equals(Constants.getRoleID.ADMIN.toString())) {
				return true;
			}
		}
		return false;
	}
	
	
}
