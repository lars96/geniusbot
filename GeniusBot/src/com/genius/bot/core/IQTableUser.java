package com.genius.bot.core;

public class IQTableUser {

	private String iq;
	private String date;

	public IQTableUser(String u, String date) {
		this.iq = u;
		this.date = date;
	}
	
	public String getIQ() {
		return iq;
	}

	public String getDate() {
		return date;
	}

	
}
