package com.genius.bot.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

import com.genius.bot.event.CommandEventArgs;
import com.genius.bot.event.ICommandListener;

import sx.blah.discord.handle.obj.IMessage;

public class Commands {

	private LinkedHashMap<String, Command> commands = new LinkedHashMap<>();
	private char delimiter = Main.commanddelimiter;
	private boolean loaded = true;
	private String[] musicbotcommands = {"blacklist", "clean", "clear", "disconnect", "id", "joinserver", "listids", "np", "pause", "perms", "play", "pldump", "queue", "restart", "resume", "search", "setavatar", "setname", "setnick", "shuffle", "shutdown", "skip", "summon", "volume", "help"};
	
	public Command registerCommand(String command, String description, ICommandListener listener) {
		command = command.trim().toLowerCase();
		Command ret = new Command(command, description, listener);
		if (commands.containsKey(command))
			commands.remove(command);
		commands.put(command, ret);
		return ret;
	}

	public Map<String, Command> getCommands() {
		return commands;
	}

	public char getDelimiter() {
		return delimiter;
	}

	public void setDelimiter(char delimiter) {
		this.delimiter = delimiter;
	}

	public void parseMessage(Bot bot, IMessage message) {
		if (loaded) {
			String msg = message.getContent();
			if (msg != null) {
				if (msg.length() > 1) {
					if (msg.charAt(0) == delimiter) {
						msg = msg.substring(1).trim();
						String[] args = msg.split("\\s+");
						if (args != null) {
							if (args.length > 0) {
								args[0] = args[0].toLowerCase();
								if (commands.containsKey(args[0])) {
									Command cmd = commands.get(args[0]);
									ArrayList<String> params = new ArrayList<>();
									for (int i = 1; i < args.length; i++)
										params.add(args[i]);
									String raw_params = "";
									if (args[0].length() < msg.length())
										raw_params = msg.substring(args[0].length(), msg.length()).trim();
										commands.get(args[0]).setOnCommand(
												new CommandEventArgs(bot, cmd, message, params, raw_params));
								}
								else {
									if (!Arrays.asList(musicbotcommands).contains(args[0])) {
										Operations.sendMessage(message.getChannel(),
											"Unbekannter Befehl `" + args[0] + "`!\n" +
											"F�r Hilfe: `" + delimiter + "helpbot`");
									}
								}	
							}
						}
					}
				}
			}
		}
	}
	
	public void sort() {
		TreeMap<String, Command> copy = new TreeMap<>(commands);
		commands.clear();
		commands.putAll(copy);
	}
}
