package com.genius.bot.core;

public class IQLeaderboardUser {

	private String user;
	private String differenceIQ;

	public IQLeaderboardUser(String u, String diff) {
		this.user = u;
		this.differenceIQ = diff;
	}
	
	public String getUser() {
		return user;
	}

	public String getDifferenceIQ() {
		return differenceIQ;
	}

	
}
