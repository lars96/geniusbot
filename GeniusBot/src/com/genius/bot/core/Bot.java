package com.genius.bot.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.format.DateTimeFormatter;
import com.genius.bot.event.CommandEventArgs;
import com.genius.bot.event.ICommandListener;
import com.genius.bot.feature.*;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelEvent;
import sx.blah.discord.handle.impl.events.user.PresenceUpdateEvent;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

public class Bot {

	public static final int MAX_DISPLAY_HELP_COMMANDS = 40;
	public static final int MAX_DISPLAY_PLAYLISTS = 10;
	public static final int MAX_DISPLAY_TRACKS = 10;
	public static final int ue = (int) '\u00FC';
	public static final int ae = (int) '\u00E4';
	public static final int oe = (int) '\u00F6';
	public static final int UE = (int) '\u00DC';
	public static final int AE = (int) '\u00C4';
	public static final int OE = (int) '\u00D6';
	public static final int SS = (int) '\u00DF';
	private Commands commands = new Commands();
	private boolean use_tts = false;
	public final Bot _bot = this;
	private boolean isRateLimited = false;
	public static IDiscordClient client;
		
	private void sendMessage(CommandEventArgs args, String text) {
		try {
			if (!isRateLimited) args.getChannel().sendMessage(text, use_tts);
		} catch (MissingPermissionsException e) {
			e.printStackTrace();
		} catch (DiscordException e) {
			e.printStackTrace();
		} catch (RateLimitException e) {
			e.printStackTrace();
			try {
				isRateLimited = true;
				long de = e.getRetryDelay();
				Thread.sleep(de + 10);
				isRateLimited = false;
				sendMessage(args, "You were rate limted! Time limit was " + de + " milliseconds");
			}
			catch (InterruptedException ie){
			}
		}
	}	

	public Bot(final IDiscordClient client) {
		Bot.client = client;
		client.getDispatcher().registerListener(this);
		Operations.initLaunchTimestamp();
		Command cmd = commands.registerCommand("hallo", "Sagt dir Hallo", new Hallo());	
		cmd.setHelp("Sagt dir Hallo. Usage: : " + commands.getDelimiter() + " $CMD$");
		
		cmd = commands.registerCommand("helpbot", "Zeigt die Hilfe an", new ICommandListener() {
			@Override
			public void onCommand(CommandEventArgs args) {
				StringBuilder sb = new StringBuilder("");
					int c = 0;
					for (String i : commands.getCommands().keySet()) {
						// sb.append("Hallu, ich bin der hilfreiche Hilfswahl. Ich bin hier um dir zu helfen. :whale:");
						sb.append("```\n");
						sb.append(commands.getDelimiter());
						sb.append(i);
						sb.append("\n\t");
						sb.append(commands.getCommands().get(i).getDescription());
						sb.append("```");
						++c;
						if ((c % MAX_DISPLAY_HELP_COMMANDS) == 0) {
							c = 0;
							sendMessage(args, sb.toString());
							sb = new StringBuilder();
							try {
								Thread.sleep(1000);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				args.getIssuer().getOrCreatePMChannel().sendMessage(sb.toString());
				sendMessage(args, "Wir haben den hilfreichen Hilfswal zu deinen DMs geschickt, er ist sehr hilfsbereit. :whale:");
				
			}
		});
		cmd.setHelp("Dieser Befehl zeigt die Hilfe an. \n\tUsage " + commands.getDelimiter() + "$CMD$ <befehl>.");
//		
//		Quit
		cmd = commands.registerCommand("quit", "Disconnectet den Bot", new Quit());
		cmd.setHelp("Disconnectet den Bot. \n\tUsage " + commands.getDelimiter() + "$CMD$ <username>.");	
//		
//		IQ
		cmd = commands.registerCommand("iq", "Ermittelt den IQ via GeniusGraph", new IQ());
		cmd.setHelp("Dieser Befehl zeigt den letzten IQ-Wert vom GeniusGraph an. \n\tUsage " + commands.getDelimiter() + "$CMD$ <username>.");
//		
//		Graph
		cmd = commands.registerCommand("graph", "Schickt den aktuellen GeniusGraph in den Chat", new Graph());
		cmd.setHelp("Dieser Befehl zeigt den aktuellen GeniusGraph an. \n\tUsage " + commands.getDelimiter() + "$CMD$ <username>.");		
//		
//		Avatar
		cmd = commands.registerCommand("avatar", "Zeigt den Avatar an", new Avatar());
		cmd.setHelp("Zeigt den Avatar. \n\tUsage " + commands.getDelimiter() + "$CMD$ <username>.");
//
//		Random
		cmd = commands.registerCommand("random", "Sucht einen zuf" + (char) ae + "lligen Titel", new RandomTrack());
		cmd.setHelp("Sucht einen zuf" + (char) ae + "lligen Titel auf Genius. \n\tUsage " + commands.getDelimiter() + "$CMD$.");	
//
//		Unread
		cmd = commands.registerCommand("unread", "Zeigt die ungelesenen Benachrichtigungen an", new Unread());
		cmd.setHelp("Zeig ungelesene Benachrichtigungen an. \n\tUsage " + commands.getDelimiter() + "$CMD$ <username>.");	
//
//		My roles
		cmd = commands.registerCommand("myroles", "Zeigt die eigenen Rollen an", new MyRoles());
		cmd.setHelp("Dieser Befehl zeigt die eigenen Rollen an. \n\tUsage " + commands.getDelimiter() + "$CMD$.");	
//
//		List
		cmd = commands.registerCommand("list", "Zeigt alle User an, die registriert sind.", new ListUsers());
		cmd.setHelp("Zeigt alle beim GeniusGraph registrierten Nutzer an. \n\tUsage " + commands.getDelimiter() + "$CMD$.");		
//
//		Slots
		cmd = commands.registerCommand("slots", "Spielt Slot Machine!", new Play());
		cmd.setHelp("Spielt die Slot Machine! Usage: " + commands.getDelimiter() + " $CMD$");
//		
//		Ping
		cmd = commands.registerCommand("ping", "Antwortet mit Pong", new Ping());
		cmd.setHelp("Sendet pong. \n\tUsage " + commands.getDelimiter() + "$CMD$.");	
//
//		Ping Pong		
		cmd = commands.registerCommand("pingpong", "Antwortet mit PongPing", new PingPong());
		cmd.setHelp("Sendet pongping. \n\tUsage " + commands.getDelimiter() + "$CMD$.");	
		//
//		Status
		cmd = commands.registerCommand("status", "Zeigt den aktuellen Bot-Status an", new Status());
		cmd.setHelp("Zeigt Bot-Status. \n\tUsage " + commands.getDelimiter() + "$CMD$.");	
//
//		Giphy
		cmd = commands.registerCommand("giphy", "Sucht mit einem Argument das erste, passende GIF aus Giphy.", new GiphyCmd());
		cmd.setHelp("Sucht s" + (char) ue + (char) SS + "e Gifs im Internet. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//
//		Top 10: Daily
		cmd = commands.registerCommand("topday", "Zeigt die aktuelle Top 10 f" + (char) ue + "r Deutschland an", new LeaderboardsDaily());
		cmd.setHelp("Zeigt Top 10. \n\tUsage " + commands.getDelimiter() + "$CMD$.");	
//
//		Top 10: Weekly
		cmd = commands.registerCommand("topweek", "Zeigt die aktuelle Top 10 f" + (char) ue + "r Deutschland an", new LeaderboardsWeekly());
		cmd.setHelp("Zeigt Top 10. \n\tUsage " + commands.getDelimiter() + "$CMD$.");	
//
//		Top 10: Monthly
		cmd = commands.registerCommand("topmonth", "Zeigt die aktuelle Top 10 f" + (char) ue + "r Deutschland an", new LeaderboardsMonthly());
		cmd.setHelp("Zeigt Top 10. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//
//		Top 10: Monthly
		cmd = commands.registerCommand("topmonth", "Zeigt die aktuelle Top 10 f" + (char) ue + "r Deutschland an", new LeaderboardsMonthly());
		cmd.setHelp("Zeigt Top 10. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//		
//		Giphy
		cmd = commands.registerCommand("giphy", "Sucht bei Giphy nach einem passenden GIF", new GiphyCmd());
		cmd.setHelp("Sucht bei Giphy nach einem passenden GIF. \n\tUsage " + commands.getDelimiter() + "$CMD$.");	
//	
//		IQTable
		cmd = commands.registerCommand("iqtable", "Zeigt die letzten 15 IQ-Werte eines Nutzers in Tausender-Schritten an", new IQTable());
		cmd.setHelp("Zeigt die letzten 15 IQ-Werte eines Nutzers in Tausender-Schritten an. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//	
//		Poll
		cmd = commands.registerCommand("poll", "Startet eine Umfrage.", new Poll());
		cmd.setHelp("Startet eine Umfrage. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//
//		Change Playing Text
		cmd = commands.registerCommand("cpl", (char) AE + "ndert den \"Playing-Text\" des Bots.", new playingtext());
		cmd.setHelp((char) AE + "ndert den \"Playing-Text\" des Bots. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//
//		Register
		cmd = commands.registerCommand("register", "Zeigt Informationen an, wie du dich f" + (char) ue + "r den GeniusGraph registrieren kannst.", new RegisterCmd());
		cmd.setHelp("Zeigt Informationen an, wie du dich f" + (char) ue + "r den GeniusGraph registrieren kannst. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//
//		Listen
		cmd = commands.registerCommand("listen", "Schiebt dich in den Voice-Channel, indem der Musik-Bot ist.", new ListenCmd());
		cmd.setHelp("Schiebt dich in den Voice-Channel, indem der Musik-Bot ist. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//
//		IQ Forecast
		cmd = commands.registerCommand("when", "Ermittelt, wann ein angegebener IQ-Wert erreicht wird.", new WhenIQ());
		cmd.setHelp("Ermittelt, wann ein angegebener IQ-Wert erreicht wird. \n\tUsage " + commands.getDelimiter() + "$CMD$ <User> <IQ>.");
//
//		Mute
		cmd = commands.registerCommand("mute", "Mutet einen Nutzer nach Angabe seiner ID. (Argument 1)", new Mute());
		cmd.setHelp("Mutet einen Nutzer nach Angabe dessen IDs. (Argument 1) \n\tUsage " + commands.getDelimiter() + "$CMD$ <User>.");
//
//		Unmute
		cmd = commands.registerCommand("unmute", "Entmutet einen Nutzer nach Angebe seiner ID. (Argument 1)", new Unmute());
		cmd.setHelp("Entmutet einen Nutzer nach Angebe seiner ID. (Argument 1) \n\tUsage " + commands.getDelimiter() + "$CMD$ <User>.");
//
//		List Muted
		cmd = commands.registerCommand("lmuted", "Listet alle gemuteten Nutzer auf.", new ListMutedUsers());
		cmd.setHelp("Listet alle gemuteten Nutzer auf. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//
//		9GAG
		cmd = commands.registerCommand("9gag", "Zeigt ein zuf" + (char) ae + "lliges Meme aus der 9GAG Hot-Liste.", new NineGag());
		cmd.setHelp("Zeigt ein zuf" + (char) ae + "lliges Meme aus der 9GAG Hot-Liste. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//
//		Flip a Coin!
		cmd = commands.registerCommand("coin", "Wirft eine M" + (char) ue + "nze!", new Coin());
		cmd.setHelp("Wirft eine M" + (char) ue + "nze! \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//
//		Base64
		cmd = commands.registerCommand("base64", "Dekodiert oder enkodiert einen Text in oder aus Base64. (Syntax: base64 <encode|decode> <text>)", new Base64());
		cmd.setHelp("Dekodiert oder enkodiert einen Text in oder aus Base64. (Syntax: base64 <encode|decode> <text>) \n\tUsage " + commands.getDelimiter() + "$CMD$. <encode|decode> <text>");
//
//		Unshorten URL
		cmd = commands.registerCommand("longurl", "Verl" + (char) ae + "ngert eine gek" + (char) ue + "rzte URL zu ihrer langen Form.", new Unshorten());
		cmd.setHelp("Verl" + (char) ae + "ngert eine gek" + (char) ue + "rzte URL zu ihrer langen Form. \n\tUsage " + commands.getDelimiter() + "$CMD$. <URL>");
//
//		Unshorten URL
		cmd = commands.registerCommand("bd", "L" + (char) oe + "scht alle Nachrichten in einem Channel.", new Bd());
		cmd.setHelp("L" + (char) oe + "scht alle Nachrichten in einem Channel. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
//		Grant
		cmd = commands.registerCommand("grant", "Gew" + (char) ae + "hrt einem user die Rechte f�r den unread-Befehl", new Grant());
		cmd.setHelp("Gew" + (char) ae + "hrt einem user die Rechte f�r den unread-Befehl. \n\tUsage " + commands.getDelimiter() + "$CMD$.");
		
		
		commands.sort();
		client.getDispatcher().registerListener(new IListener<ReadyEvent>() {
			@Override
			public void handle(ReadyEvent event) {
				Operations.setOwnGuild(client.getGuilds().get(0));
				Operations.setOwnUser(client.getOurUser());
				System.out.println("[DiscordClient@" + client.getOurUser().getStringID() + "] INFO com.genius.bot.core.Bot - API is ready!");

			}
		});
		
		client.getDispatcher().registerListener(new IListener<MessageReceivedEvent>() {
			@Override
			public void handle(MessageReceivedEvent event) {
					System.out.println("[" + event.getMessage().getCreationDate().format(DateTimeFormatter.ofPattern("dd.MM.yyyy - kk:mm:ss")) + "] <" + event.getMessage().getAuthor().getName() + "@"
									+ event.getMessage().getChannel().getName() + ">: " + event.getMessage().getContent());
				if (event.getClient().getOurUser().getStringID().compareTo(event.getMessage().getAuthor().getStringID()) != 0 && !event.getChannel().isPrivate()) {
					commands.parseMessage(_bot, event.getMessage());
				}
				File mutes = new File(System.getProperty("user.dir") + File.separator + "mutes.dat");
				if(mutes.exists()) {
					try (BufferedReader br = new BufferedReader(new FileReader("mutes.dat"))) {
					    String crl;
					    while ((crl = br.readLine()) != null) {
					    	if(String.valueOf(event.getAuthor().getLongID()).equals(crl)) {
					    		event.getMessage().delete();
					    		break;
					    	}
					    }
					    br.close();
		    	   } catch(Exception ex) {
		    		   ex.printStackTrace();
		    	   }
				}
			}
		});
		
		client.getDispatcher().registerListener(new IListener<PresenceUpdateEvent>() {

			@Override
			public void handle(PresenceUpdateEvent userJoined) {
				Operations.autoAssignRole(userJoined);
			}
		});
		
		client.getDispatcher().registerListener(new IListener<UserVoiceChannelEvent>() {
			@Override
			public void handle(UserVoiceChannelEvent arg0) {
				Operations.handleVoiceChatEvent(arg0);
			}
		});
		
	}

	public Commands getCommands() {
		return commands;
	}
	
	public static Bot login(String token) {
		Bot bot = null;

		ClientBuilder builder = new ClientBuilder();
		builder.withToken(token);
		try {
			IDiscordClient client = builder.login();
			bot = new Bot(client);
		} catch (DiscordException e) {
			System.err.println("Error occurred while logging in!");
			e.printStackTrace();
		}

		return bot;
	}

}
