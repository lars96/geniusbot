package com.genius.bot.core;

import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class LeaderboardParser {

	public static String parseLeaderboards(String period){
		return parseLeaderboards(period, 1658);
		
	}
	public static String parseLeaderboards(String period, int tag)
	{
		List<String> iqEarned = new ArrayList<String>();
		String pos = "```";
		try {
//			String url = "https://genius.com/tags/1658/leaderboard?period=" + period;
			String url = "https://genius.com/tags/" + tag + "/leaderboard?period=" + period;
			org.jsoup.nodes.Document d = Jsoup.connect(url).post();
			
			Elements iqEarned_elements = d.body().select("td[class^=leaderboard-sort_value]");
			for (Element element : iqEarned_elements) {
				iqEarned.add(element.text());
			}
			//Users
			Elements elements = d.body().select("div[class^=user_badge]");
			int i = 0;
			for (Element element : elements) {
				String uni_user = element.getElementsByClass("user_details").first().getElementsByClass("login").first().attr("data-id");
				String user = Operations.stripOffNonPrintableCharacters(uni_user);
						
//				System.out.println(Operations.escapeUnicode(user));
				pos += i < 9 ? " " : "";
				pos += String.valueOf(i + 1) + ". ";
				
//				pos += user + " (+" + iqEarned.get(i) + " IQ)\n";
				pos += Operations.formatLeaderboards(user, iqEarned.get(i)) + " IQ\n";
				i++;
			}
			pos += "```";
		} catch (Exception e) {
			pos += e.getMessage();
		}
		return pos;
	}
	

}
