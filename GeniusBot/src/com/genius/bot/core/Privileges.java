package com.genius.bot.core;

import java.util.List;
import com.genius.bot.event.CommandEventArgs;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

public class Privileges {
	
	public static boolean checkBotUserPrivileges(CommandEventArgs e){
		List<IRole> li = e.getIssuer().getRolesForGuild(e.getChannel().getGuild());
		return checkBotUserPrivileges(li);
	}

	public static boolean checkBotUserPrivileges(List<IRole> li){
		for(IRole i : li){
			if (i.getStringID().equals(Constants.getRoleID.BOT_USER.toString())) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean checkAdminPrivileges(CommandEventArgs e){
		return checkAdminPrivileges(e.getIssuer());
	}
	
	public static boolean checkAdminPrivileges(IUser user) {
		List<IRole> li = user.getRolesForGuild(Operations.getOwnGuild());
		for(IRole i : li){
			if (i.getStringID().equals(Constants.getRoleID.ADMIN.toString())) {
				return true;
			}
		}
		return false;
	}
	
	
}
